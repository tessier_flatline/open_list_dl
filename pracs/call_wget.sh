#!/bin/bash

# so this kind of handles link checking...

URLREGEX='(https?|https|http|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'

STRING1='https://i.redditmedia.com/1xvVRXXrt-r8jsXENf7uNZw917aBWBERX1SwLIQTk88.jpg?w=576&s=7767717dbb4f425089f8f7d288c7e46f'
STRING2='not_a_link'

if [[ $STRING1 =~ $URLREGEX ]]
then 
    echo "Link1 valid"
else
    echo "Link1 not valid"
fi

if [[ $STRING2 =~ $URLREGEX ]]
then 
    echo "Link2 valid"
else
    echo "Link2 not valid"
fi

#########

DIR=$(pwd)

# wget output file
FILE=somecoolname.`date +"%Y%m%d"`

# wget log file
LOGFILE=wget.log

# wget download url
URL=$STRING1

cd $DIR
wget $URL -O $FILE -o $LOGFILE
