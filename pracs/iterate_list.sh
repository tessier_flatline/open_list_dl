#!/bin/bash

# testing iterating links

# provided file arg
FNAME=$1

# parse current working directory
WORKPATH=$(pwd)
echo $WORKPATH

ABSPATH="${WORKPATH}/$FNAME"
echo $ABSPATH

# loop to print lines - uses carriage return
while IFS= read -r var
do
  echo "$var"
done < $ABSPATH
