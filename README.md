Open List Downloader
---

I'm a data-hoarder, therefore I have odd problems when it comes to shoving as much data into my fileserver as *digitally* possible.

The whole idea behind this script is to take a list of download links and...:

* create a folder as indicated at the top of the list file
* increment each link with a name pulled from wget AND or incremented from folder name
    * this might benefit from some super basic fuzzy matching?
* include a parsed "summary" written at the bottom of the list of files
* include a "tags" file of parsed "tags" from the bottom of the list of files
    * not sure what format this should be, it'll likely just be JSON?
    
---

Objectives:

* create basic prototype [ ]

---